<?php
/*
 * Gemini server written in PHP by seven@0xm.net
 * Version 0.1, Oct 2020
*/

date_default_timezone_set('America/Argentina/Buenos_Aires');
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE);
ini_set('display_errors', 'Off');
ini_set('display_startup_errors', 'Off');
ini_set("error_log", "./server.log");

if(!require("config.php")) {
	die("config.php is missing.  Copy config.php.sample to config.php and customise your settings");
}
require_once("gemini.class.php");
require_once("core.php");

try {
	$g = new Gemini($config);

	$context = stream_context_create();

	stream_context_set_option($context, 'ssl', 'local_cert', $g->certificate_file);
	//stream_context_set_option($context, 'ssl', 'passphrase', $g->certificate_passphrase);
	stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
	stream_context_set_option($context, 'ssl', 'verify_peer', false);
	$socket = stream_socket_server("tcp://{$g->ip}:{$g->port}", $errno, $errstr, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN, $context);
	if($socket) {
		stream_socket_enable_crypto($socket, false);

		// apply patch from @nervuri:matrix.org to stop supporting out of spec versions of TLS
		$cryptoMethod = STREAM_CRYPTO_METHOD_TLS_SERVER
			& ~ STREAM_CRYPTO_METHOD_TLSv1_0_SERVER
			& ~ STREAM_CRYPTO_METHOD_TLSv1_1_SERVER;

		error_log("Connected.");

		while(true) {
			$forkedSocket = stream_socket_accept($socket, "-1", $remoteIP);
			if($forkedSocket) {
				stream_set_blocking($forkedSocket, true);
				stream_socket_enable_crypto($forkedSocket, true, $cryptoMethod);
				$line = fread($forkedSocket, 1024);
				stream_set_blocking($forkedSocket, false);

				$parsed_url = $g->parse_request($line);
				$filepath = $g->get_filepath($parsed_url);
				$status_code = $g->get_status_code($filepath);

				if($filepath == 'hosts/default/index.gemini') {

					$filesize = 0;

					if($status_code == "20") {
						$objCore = new Core();
						$meta = SYS_META_MEDIA_TYPE;
						$content = file_get_contents($filepath);
						$qotd = $objCore->GetQOTD();
						$content = str_replace("{qotdquote}", $qotd->cita, $content);
						$content = str_replace("{qotdauthor}", $qotd->autor, $content);
						$content = str_replace("{fecha}", date('d/m/Y'), $content);
						$filesize = strlen($content);
					}
					else {
						$meta = SYS_MSG_NOT_FOUND;
					}
				}
				else {
					$meta = "";
					$filesize = 0;

					if($status_code == "20") {
						$meta = SYS_META_MEDIA_TYPE;
						$content = file_get_contents($filepath);
						$filesize = filesize($filepath);
					}
					else {
						$meta = SYS_MSG_NOT_FOUND;
					}
				}

				$status_line = $status_code . " " . $meta . "\r\n";

				fwrite($forkedSocket, $status_line);
				if($status_code == "20") {
					fwrite($forkedSocket, $content);
				}

				fclose($forkedSocket);
				//sleep(1);
			}
		}

		fclose($socket);

		exit("Disconnected.");
	}
	else {
		error_log("Cannot bind to {$g->ip}:{$g->port}: " . $errstr);
	}
}
catch(exception $err) {
	error_log($err);
}

?>
