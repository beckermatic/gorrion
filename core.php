<?php

require_once('config.php');

class Core {

	public function GetQOTD() {
		try {
			$this->_initializeDBConn();

			$dia = date('d');
			$mes = date('m');

			$sqlQuery = "SELECT * FROM " . DB_TABLE_QOTD . " WHERE dia = " . $dia . " AND mes = " . $mes;
			$sqlStatement = $this->sqlDBHandler->prepare($sqlQuery);
			$sqlStatement->execute();
			$retValue = $sqlStatement->fetch(PDO::FETCH_OBJ);
		}
		catch(exception $e) {
			error_log('Error PDO: ' . $e);
			if($boolComplete) {
				$retValue = null;
			}
			else {
				$retValue = 0;
			}
		}
		$sqlStatement = null;
		$this->_destroyDBConn();
		return $retValue;
	}

	private function _initializeDBConn() {
		try {
		/*
CREATE USER 'burners_usr'@'localhost' IDENTIFIED WITH mysql_native_password BY 'kv7j9«88.sE^k5';
GRANT ALL ON *.* TO 'burners_usr'@'localhost';
		*/
			$mysql_hostname = DB_SERVER;
			$mysql_dbname = DB_NAME;
			$mysql_password = DB_PASSWORD;
			$mysql_username = DB_USER;
			$this->sqlDBHandler = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname;charset=utf8", $mysql_username, $mysql_password);
			$this->sqlDBHandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			$sqlQuery = "SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));";
			$sqlStatement = $this->sqlDBHandler->prepare($sqlQuery);
			$sqlStatement->execute();

			return true;
		}
		catch(exception $e) {
			error_log('Error PDO: ' . $e);
			return false;
		}
	}

	private function _destroyDBConn() {
		try {
			$this->sqlDBHandler = null;
			return true;
		}
		catch(exception $e) {
			error_log('Error PDO: ' . $e);
			return false;
		}
	}
}
?>
