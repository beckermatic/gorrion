<?php

	/*
	 * Set the location of your certificate file.  All other settings are optional.
	 *
	 *
	 * This is your certificate file.  A self-signed certificate is acceptable here.
	 * You can generate one using:
	 *   
	 *   openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365
	 *
	 * Then combine the key and certificate and copy them to the certs directory:
	 *
	 *  cp cert.pem certs/yourdomain.com.pem
	 *  cat key.pem >> certs/yourdomain.com.pem

	 * Enter the passphrase (if you used one) below.
	 *
	 */

	define('SYS_MSG_NOT_FOUND', 'No se encontró el recurso');
	define('SYS_META_MEDIA_TYPE', 'text/gemini; lang=en');

	define("DB_SERVER", "localhost");
	define("DB_PORT", "3306");
	define("DB_NAME", "arielbecker_mastodon");
	define("DB_USER", "user");
	define("DB_PASSWORD", "xzaqh548");
	define("DB_TABLE_QOTD", "QOTD");

	$config['certificate_file'] = 'certs/cert.pem';
	$config['certificate_passphrase'] = '';

	// IP address to listen to (leave commented out to listen on all interfaces)
	$config['ip'] = "192.168.1.100";

	// Port to listen on (1965 is the default)
	$config['port'] = "1965";

	// This is the location files are served from
	// The path is constructed based on the hostname in the client request
	// i.e. gemini://domain1.com/file1 is retrieved from hosts/domain1.com/file1
	$config['data_dir'] = "hosts/";
	$config['default_host_dir'] = "default/";

	// Default index file.  If a path isn't specified then the server will
	// default to an index file (like index.html on a web server).
	$config['default_index_file'] = "index.gemini";

	// Logging, setting this to false will disable logging (default is on/true);
	$config['logging'] = true;
	$config['log_file'] = "logs/gemini-php.log";

?>
